import React from 'react';

import {
  Chart,
  Tooltip,
  Interval,
  Area,
  View,
  Line,
  Coordinate,
  Axis,
  // InteractionAction,
  Interaction,
  Point,
  Legend,
} from 'bizcharts';
import { Row, Col, Card } from 'antd';

// import ReactParticleLine from 'react-particle-line';

// import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import { GridContent } from '@ant-design/pro-layout';

const areaData = [
  {
    month: '1月',
    value: 55.52,
    amount: 9,
  },
  {
    month: '2月',
    value: 71.23,
    amount: 4,
  },
  {
    month: '3月',
    value: 71.71,
    amount: 2,
  },
  {
    month: '4月',
    value: 76.98,
    amount: 11,
  },
  {
    month: '5月',
    value: 33.98,
    amount: 7,
  },
  {
    month: '6月',
    value: 22.21,
    amount: 10,
  },
  {
    month: '7月',
    value: 93.11,
    amount: 5,
  },
  {
    month: '8月',
    value: 14.95,
    amount: 2,
  },
  {
    month: '9月',
    value: 34.88,
    amount: 7,
  },
];

const data = [
  { name: '累计执金额', department: '部门A', amount: 55.52 },
  { name: '累计执金额', department: '部门B', amount: 71.23 },
  { name: '累计执金额', department: '部门C', amount: 152.64 },
  { name: '累计执金额', department: '部门D', amount: 112.08 },
  { name: '累计执金额', department: '部门E', amount: 33.98 },
  { name: '累计执金额', department: '部门F', amount: 49.12 },

  { name: '累计执行数量', department: '部门A', amount: 5 },
  { name: '累计执行数量', department: '部门B', amount: 9 },
  { name: '累计执行数量', department: '部门C', amount: 11 },
  { name: '累计执行数量', department: '部门D', amount: 22 },
  { name: '累计执行数量', department: '部门E', amount: 3 },
  { name: '累计执行数量', department: '部门F', amount: 7 },
];

const dataCircle = [
  {
    type: '部门A',
    percent: 11.7,
  },
  {
    type: '部门B',
    percent: 15.01,
  },
  {
    type: '部门C',
    percent: 32.16,
  },
  {
    type: '部门D',
    percent: 23.62,
  },
  {
    type: '部门E',
    percent: 7.16,
  },
  {
    type: '部门F',
    percent: 10.35,
  },
];

const scale = {
  value: {
    min: 10000,
    nice: true,
    alias: '累计执行金额',
    tickCount: 5,
  },
  year: {
    range: [0, 1],
  },
  amount: {
    alias: '累计执行数量',
    tickCount: 5,
  },
};

export default function Progress() {
  return (
    // <ReactParticleLine>
    //   <ReactCSSTransitionGroup>
    <GridContent>
      <Card>
        <Row
          gutter={24}
          style={{
            marginTop: 24,
          }}
          justify="start"
        >
          <Chart
            height={300}
            padding="auto"
            data={data}
            autoFit
            scale={{
              department: {
                alias: '部门',
                tickCount: 5,
              },
              amount: {
                alias: '数量',
                tickCount: 5,
              },
            }}
          >
            <Interval
              adjust={[
                {
                  type: 'dodge',
                  marginRatio: 0,
                },
              ]}
              color="name"
              position="department*amount"
              label="amount"
              yField="value"
            />
            <Tooltip shared />
          </Chart>
        </Row>
      </Card>
      <Row
        gutter={24}
        style={{
          marginTop: 24,
        }}
      >
        <Col xl={12} lg={12} md={24} sm={24} xs={24}>
          <Card>
            <Chart
              data={dataCircle}
              height={300}
              autoFit
              title={{
                visible: true,
                text: '堆叠柱状图：label自动隐藏',
              }}
              description={{
                visible: true,
                text:
                  '在堆叠柱状图中，如果label的位置被设定为middle，即显示在柱形中间。在对应柱形大小不够摆放label的情况下，label会被自动隐藏。',
              }}
              scale={{
                percent: {
                  formatter: (val: any) => {
                    // eslint-disable-next-line no-param-reassign
                    val = `${val}%`;
                    return val;
                  },
                },
              }}
            >
              <Coordinate type="theta" radius={0.6} innerRadius={0.58} />
              <Axis visible={false} />
              <Tooltip showTitle={false} />
              <Interval adjust="stack" position="percent" color="type" shape="sliceShape" />
              <Interaction type="element-single-selected" />
              <Legend
                layout="vertical"
                position="right"
                offsetX={-55}
                itemHeight={40}
                itemName={{
                  spacing: 10, // 文本同滑轨的距离
                  style: {
                    fontSize: 14,
                  },
                  formatter: (text, item) => {
                    return `${text}:  ${
                      (dataCircle.find((r) => r.type === item.id) as any).percent
                    }%`;
                  },
                }}
              />
            </Chart>
          </Card>
        </Col>

        <Col xl={12} lg={12} md={24} sm={24} xs={24}>
          <Card>
            <Chart scale={scale} height={300} data={areaData} autoFit>
              <Area position="month*value" color="#5AD8A6" />
              <View data={areaData} label="value">
                <Axis visible={false} />
                <Line position="month*amount" />
                <Point position="month*amount" size={3} shape="circle" />
              </View>
            </Chart>
          </Card>
        </Col>
      </Row>
    </GridContent>
    //   </ReactCSSTransitionGroup>
    // </ReactParticleLine>
  );
}
