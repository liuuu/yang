import React, { useState } from 'react';
import styles from '../styles/index.css';
import MainLevel from './MainLevel';
import SubLevel from './SubLevel';

import { ProgressItemProps } from './interface';

const ProgressItem = ({
  data,
  index,
  len,
}: {
  data: ProgressItemProps;
  index: number;
  len: number;
}) => {
  // 子节点收缩与否
  const [collapse, setCollapse] = useState(false);
  const { subNodes, } = data
  const current = index === 0;

  const last = index === len - 1;

  return (
    <div className={styles.itemWrap}>
      <MainLevel data={data} collapse={collapse} setCollapse={setCollapse} current={current} />
      <SubLevel
        subNodes={subNodes}
        collapse={collapse}
        last={last}
      />
    </div>
  );
};

export default ProgressItem;
