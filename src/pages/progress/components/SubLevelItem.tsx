import React from 'react';
import styles from '../styles/index.css';
import { SubNode } from './interface';

export default function SubLevelItem({ data }: { data: SubNode }) {
  const { title, date, time, day } = data
  return (
    <div className={styles.subLiWrap}>
      <PointDom title={title} />
      <Status date={date} time={time} day={day} />
    </div>
  );
}

function PointDom({ title }: { title: string }) {
  return (
    <div className={styles.subLi}>
      <div className={styles.point}></div>
      <div className={styles.subLiText}>{title}</div>
    </div>
  );
}

function Status({ date, time, day}: any) {
  return (
    <div className={styles.statusWrap}>
      <span className={styles['status-desc']}>{date}</span>
      <span className={`${styles['status-desc']} ${styles.day}`}>{day}</span>
      <span className={styles['status-desc']}>{time}</span>
    </div>
  );
}
