import React, { useMemo } from 'react'; // useEffect, 

import { ProgressItemProps } from './interface';
import foldImg from '../assets/fold.png';
import unFoldImg from '../assets/unfold.png';
import firstLevelDone from '../assets/point1.png';
import firstLevelTodo from '../assets/point2.png';
import styles from '../styles/index.css';

export default function MainLevel({
  data,
  collapse,
  setCollapse,
  current,
}: {
  data: ProgressItemProps;
  collapse: boolean;
  setCollapse: Function;
  current: any;
}) {
  const { title, subNodes } = data;

  // 展示展开收缩
  const showCollapse = useMemo(()=>!!(Array.isArray(subNodes) && subNodes.length),[subNodes]);

  // 展开第一个节点的子节点
  // useEffect(() => {
  //   if (current && showCollapse) {
  //     setCollapse(!collapse);
  //   }
  // }, [current, showCollapse]);

  return (
    <div className={styles.firstLevelWrap}>
      <div className={styles.firstCenterIcon}>
        <img src={current ? firstLevelDone : firstLevelTodo} alt="icon" className={styles.icon} />
      </div>
      <div className={styles.firstRightContent}>
        <p className={styles.firstName}>{title}</p>
        <div className={styles.firstContentExtra}>
          {showCollapse && (
            <div className={styles['collapse-btn-wrap']} onClick={() => setCollapse(!collapse)}>
              <div>{collapse ? '展开' : '收起'}</div>
              <img
                src={collapse ? unFoldImg : foldImg}
                alt=""
                className={styles['collapse-icon']}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
