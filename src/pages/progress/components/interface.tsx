export interface SubNode {
  title: string;
  logTime: string;
  key?: number;
  date: string;
  time: string;
  day: string;
}

export interface ProgressItemProps {
  title: string;
  logTime?: string;
  subNodes: SubNode[];
  key?: number;
}
