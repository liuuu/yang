import React from 'react';
import SubLevelItem from './SubLevelItem';
import { SubNode } from './interface';
import styles from '../styles/index.css';

export default function SubLevel({
  subNodes,
  collapse,
  last,
}: {
  subNodes: SubNode[];
  collapse: boolean;
  last: boolean;
}) {
  
  return (
    <div className={`${styles.subLevel} ${collapse && last ? styles.subLevelBorderNone : ''}`}>
      {!collapse && subNodes.map((node) => <SubLevelItem data={node} key={node.key} />)}
    </div>
  );
}
