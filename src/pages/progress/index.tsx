import React from 'react';
import { Card } from 'antd';
import ProgressItem from './components/progressItem';
import { ProgressItemProps } from './components/interface';

import progresses from './data/index';

const Progress = () => {
  // 一级节点长度
  const len = Array.isArray(progresses) ? progresses.length : 0;
  return (
    <Card title="进度详情">
      {!!len
        ? progresses.map((progress: ProgressItemProps, index: number) => {
            return <ProgressItem data={progress} index={index} key={progress.key} len={len} />;
          })
        : null}
    </Card>
  );
};

export default Progress;
