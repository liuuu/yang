import { ProgressItemProps, SubNode } from '../components/interface';

const progresses: any = [
  {
    title: '已完结',
    logTime: '',
    subNodes: [
      {
        title: '新增固定资产，编号：******',
        logTime: '2020/11/28 15:50:00',
      },
      {
        title: '已收回发票，支付验收款',
        logTime: '2020/11/27 15:50:00',
      },
      {
        title: '已转入部门使用',
        logTime: '2020/11/25 15:50:00',
      },
    ],
  },
  {
    title: '验收合格',
    logTime: '',
    subNodes: [
      {
        title: '正式验收合格',
        logTime: '2020/11/24 15:50:00',
      },
      {
        title: '校准合格',
        logTime: '2020/11/23 15:50:00',
      },
      {
        title: '整改完毕',
        logTime: '2020/11/20 15:50:00',
      },
      {
        title: '校准不合格，要求整改',
        logTime: '2020/11/16 15:50:00',
      },
      {
        title: '校准合格',
        logTime: '2020/11/16 15:50:00',
      },
      {
        title: '初验合格',
        logTime: '2020/11/14 15:50:00',
      },
      {
        title: '整改完毕',
        logTime: '2020/11/14 15:50:00',
      },
      {
        title: '初验不合格，要求整改',
        logTime: '2020/11/10 15:50:00',
      },
      {
        title: '初验合格',
        logTime: '2020/11/10 15:50:00',
      },
    ],
  },
  {
    title: '签订合同，支付货款，跟踪交付',
    logTime: '',
    subNodes: [
      {
        title: '已支付首付款',
        logTime: '2020/10/30 15:50:00',
      },
      {
        title: '资产采购合同已签订',
        logTime: '2020/10/28 10:10:00',
      },
    ],
  },
  {
    title: '招采工作组定标',
    logTime: '',
    subNodes: [
      {
        title: '定标报告已签发',
        logTime: '2020/10/26 10:10:00',
      },
      {
        title: '定标结束',
        logTime: '2020/10/23 10:10:00',
      },
      {
        title: '提请招采工作组定标',
        logTime: '2020/10/20 10:10:00',
      },
    ],
  },
  {
    title: '评价会议',
    logTime: '',
    subNodes: [
      {
        title: '会议结束，形成评价报告',
        logTime: '2020/10/19 10:10:00',
      },
      {
        title: '会议开始',
        logTime: '2020/10/19 10:10:00',
      },
      {
        title: '接收报价文件时间结束',
        logTime: '2020/10/19 10:10:00',
      },
    ],
  },
  {
    title: '文件发布，接受供方报名和询问',
    logTime: '',
    subNodes: [
      {
        title: '报名及答疑时间结束',
        logTime: '2020/10/18 17:10:00',
      },
      {
        title: '已发布',
        logTime: '2020/10/13 9:10:00',
      },
    ],
  },
  {
    title: '编制询价计划及询价文件',
    logTime: '',
    subNodes: [
      {
        title: '已批准，即将发布',
        logTime: '2020/10/13 9:10:00',
      },
      {
        title: '编制完成，等待招采工作组批准',
        logTime: '2020/10/12 9:10:00',
      },
      {
        title: '拟定参与供方',
        logTime: '2020/10/11 9:10:00',
      },
    ],
  },
  {
    title: '决议单',
    logTime: '',
    subNodes: [
      {
        title: '决议单已签发，详见邮件',
        logTime: '2020/10/9 10:10:00',
      },
    ],
  },
  {
    title: '院长办公会批准',
    logTime: '',
    subNodes: [
      {
        title: '院长办公会有反馈意见，详见决议单',
        logTime: '2020/9/28 10:10:00',
      },
      {
        title: '院长办公会无反馈意见，详见决议单',
        logTime: '2020/9/28 10:10:00',
      },
    ],
  },
  {
    title: '学术委员会论证',
    logTime: '',
    subNodes: [
      {
        title: '学术委员会无反馈意见，将提交院长办公会',
        logTime: '2020/9/23 15:10:00',
      },
      {
        title: '学术委员会有反馈意见，请回复邮件',
        logTime: '2020/9/21 15:10:00',
      },
      {
        title: '学术委员会无反馈意见，将提交院长办公会',
        logTime: '2020/9/21 15:10:00',
      },
      {
        title: '已提交学术委员会，等待论证反馈',
        logTime: '2020/9/15 15:08:00',
      },
    ],
  },
];

const weeks = ['周日','周一','周二','周三','周四','周五','周六']

function addKey(data: any) {
  data.key = Math.random();
  if (data.subNodes) {
    data.subNodes.forEach((subNode: SubNode) => {
      addKey(subNode);
    });
  }
  if(data.logTime){
    data.date = data.logTime.slice(0,9);
    data.time = data.logTime.slice(10);
    data.day = weeks[new Date(data.logTime).getDay()]
  }
}

export default progresses.map((item: ProgressItemProps) => {
  addKey(item);
  return item;
});
