import { Settings as ProSettings } from '@ant-design/pro-layout';

type DefaultSettings = ProSettings & {
  pwa: boolean;
};

const proSettings: DefaultSettings = {
  "navTheme": "light",
  "primaryColor": "#1890ff",
  "layout": "top",
  "contentWidth": "Fluid",
  "fixedHeader": false,
  "fixSiderbar": true,
  "menu": {
    "locale": true
  },
  "title": "progress system",
  "pwa": false,
  "iconfontUrl": ""
};

export type { DefaultSettings };

export default proSettings;
